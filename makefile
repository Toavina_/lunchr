# Makefile

help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Quick-start Commands:"
	@echo " start                       Create and start containers"
	@echo " start-f                     Create and start containers without daemon option"
	@echo " stop                        Stop containers"

start:
	@docker-compose up -d

start-f:
	@docker-compose up

stop:
	@docker-compose down

clear-cache:
	@docker-compose run --rm php php bin/console cache:clear

create-migration:
	@docker-compose run --rm php php bin/console make:migration

migrate:
	@docker-compose run --rm php php bin/console doctrine:migration:migrate

debug-config:
	@docker-compose run --rm php php bin/console config:dump-reference security

fixtures:
	@docker-compose run --rm php php bin/console make:fixtures

load-fixtures-append:
	@docker-compose run --rm php php bin/console doctrine:fixtures:load --append

load-fixtures:
	@docker-compose run --rm php php bin/console doctrine:fixtures:load